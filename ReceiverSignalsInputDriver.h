#ifndef RECEIVERSIGNALSINPUTDRIVER_H
#define RECEIVERSIGNALSINPUTDRIVER_H

#include <QObject>
#include <QThread>

#include <ReceiverSignalsInterface.h>
#include <ReceiverSignalsInputDevice.h>

class ReceiverSignalsInputDriver : public QObject
{
  Q_OBJECT

public:
  explicit ReceiverSignalsInputDriver(QObject* parent = 0);
  ~ReceiverSignalsInputDriver();

  ReceiversSignalsInputDevice* device(){return &Device;}

  void addInterface(ReceiverSignalsInterface* Interface);
  void removeInterface(ReceiverSignalsInterface* Interface);

signals:
  void setHostAddress(QHostAddress addr);
  void connected();
  void disconnected();

public slots:
  void enable(QString hostAddr);
  void disable();

private:
  QList<ReceiverSignalsInterface*> Interfaces;
  ReceiversSignalsInputDevice Device;
  QThread Thread;
};

#endif // RECEIVERSIGNALSINPUTDRIVER_H
