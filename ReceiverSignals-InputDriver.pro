#-------------------------------------------------
#
# Project created by QtCreator 2016-09-01T13:53:23
#
#-------------------------------------------------

QT       += network

QT       -= gui

TARGET = NavdatSignalsInputDriver
TEMPLATE = lib
CONFIG += dll

exists(../ReceiverSignalsVars):
{
  include(../ReceiverSignalsVars.pri)
  DESTDIR = $$RECEIVER_SIGNALS_DESTDIR
}

DEFINES += RECEIVERSSIGNALSINPUTDEVICE_LIBRARY

SOURCES += \
    ReceiverSignalsInterface.cpp \
    ReceiverSignalsInputDriver.cpp \
    ReceiverSignalsNavdatInterface.cpp \
    ReceiverSignalsInputDevice.cpp

HEADERS += \
        ReceiverSignalsInterface.h \
    ReceiverSignalsInputDriver.h \
    ReceiverSignalsNavdatInterface.h \
    ReceiverSignalsInputDevice.h \
    receiversignalsinputdevice_global.h

unix {
    target.path = /usr/local/lib
    INSTALLS += target
}
