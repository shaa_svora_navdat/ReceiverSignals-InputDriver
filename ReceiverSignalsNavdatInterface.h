#ifndef RSNAVDATINTERFACE_H
#define RSNAVDATINTERFACE_H

#include <ReceiverSignalsInterface.h>

#include "receiversignalsinputdevice_global.h"

class RECEIVERSSIGNALSINPUTDEVICESHARED_EXPORT ReceiverSignalsNavdatInterface : public ReceiverSignalsInterface
{
  Q_OBJECT

public:
  enum InformationStream {MIS, TIS, DS};
  enum SignalType {Scope, SyncEstimations, Spectrum, SignalDetected, Estimations, Equalizer, PhaseOffset, Constellation, signalsCount};
  enum SymbolType {qamBuf = signalsCount, encBuf, dataBuf, symbolsCount};
  explicit ReceiverSignalsNavdatInterface(QObject* parent = 0);

  static QByteArray convertScopeSpecParamsData(quint32 Count);

signals:
  void ConfigRequestData(quint32 Fd, quint32 Nfft);
  void ScopeData(QVector<float> iqSamples);
  void SyncEstimationsData(QVector<float> Correlation, QVector<float> FreqEstimation);
  void SpectrumData(QVector<float> iqSamples);
  void SignalDetectedData(quint32 startIdx);
  void EstimationsData(float snr, float freqOffset);
  void EqualizerData(QVector<float> Samples);
  void PhaseOffsetData(QVector<float> Samples);
  void ConstellationData(QVector<float> iqSamples);
  void qamData(quint8 xS, QVector<qint8> data);
  void encData(quint8 xS, QVector<quint8> data);
  void Data(quint8 xS, QVector<quint8> data);

private:
  void read(quint8 type, QDataStream& in);
};

#endif // RSNAVDATINTERFACE_H
