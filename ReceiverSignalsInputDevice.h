#ifndef RECEIVERSSIGNALSINPUTDEVICE_H
#define RECEIVERSSIGNALSINPUTDEVICE_H

#include <QObject>
#include <QUdpSocket>
#include <QHostAddress>

#include "receiversignalsinputdevice_global.h"

class RECEIVERSSIGNALSINPUTDEVICESHARED_EXPORT ReceiversSignalsInputDevice : public QObject
{
  Q_OBJECT

public:
  explicit ReceiversSignalsInputDevice(QObject *parent = 0);

  void setHostAddr(QString addr);
  void setHostAddr(QHostAddress addr){HostAddr = addr;}

  bool connectToServer();
  void disconnectFromServer();

  void sendQueryToHost(quint8 type);
  void sendQueryToServer(quint8 id, quint8 type, QByteArray data = QByteArray());

signals:
  void enabled();
  void ready_read(quint8 id, quint8 type, QByteArray data);
  void disabled();

public slots:
  void enable();
  void disable();

private:
  QUdpSocket *SocketIn, *SocketOut;
  QHostAddress MulticastAddr, HostAddr;
  quint16 MulticastPort, HostPort;

  bool HostIsEnabled, ServerIsConnected;

  void message(QString src, QString msg);
  inline void SocketInErrorMessage(QString src){message(src, SocketIn->errorString());}
  inline void SocketOutErrorMessage(QString src){message(src, SocketOut->errorString());}

private slots:
  void read();
};

#endif // RECEIVERSSIGNALSINPUTDEVICE_H
