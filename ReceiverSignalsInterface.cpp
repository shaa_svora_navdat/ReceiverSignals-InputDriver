#include "ReceiverSignalsInterface.h"

ReceiverSignalsInterface::ReceiverSignalsInterface(quint8 ChannelId, ReceiversSignalsInputDevice* Device,  QObject* parent) :
  QObject(parent)
{
  Id = ChannelId;
  dev = Device;
  isConnected = false;

  qRegisterMetaType<QVector<float> >("QVector<float>");
  qRegisterMetaType<ReceiverSignalsInterface::CommonSignalParams>("ReceiverSignalsInterface::CommonSignalParams");
}

void ReceiverSignalsInterface::enable()
{
  dev->sendQueryToServer(Id, ReceiverSignalsInterface::queryEnableChannel);
}

void ReceiverSignalsInterface::disable()
{
  isConnected = false;
  dev->sendQueryToServer(Id, ReceiverSignalsInterface::queryDisableChannel);
}

void ReceiverSignalsInterface::setSignalEnabled(quint8 signalType, bool enabled)
{
  QByteArray data;
  data.append(signalType);
  data.append(enabled);
  dev->sendQueryToServer(Id, ReceiverSignalsInterface::querySetSignalEnabled, data);
}

void ReceiverSignalsInterface::setSignalParams(quint8 signalType, ReceiverSignalsInterface::CommonSignalParams params, QByteArray spec_data)
{
  QByteArray data;
  data.append(signalType);
  data.append(params.mode);
  data.append(spec_data);
  dev->sendQueryToServer(Id, ReceiverSignalsInterface::querySetSignalParams, data);
}


void ReceiverSignalsInterface::read(quint8 id, quint8 type, QByteArray data)
{
  QDataStream in(&data, QIODevice::ReadOnly);
  in.setVersion(QDataStream::Qt_5_5);

  if (Id != id) return;

  switch (type)
  {
  case requestChannelEnabled:
    isConnected = true;
    emit enabled();
    break;

  case requestChannelDisabled:
    isConnected = false;
    emit disabled();
    break;
  }

  read(type, in);
  emit data_readed();
}

QVector<float> ReceiverSignalsInterface::read_samples(QDataStream& in)
{
  quint32 count = 0;
  in >> count;
  QVector<float> samples;
  samples.resize(count);
  for (int i = 0; i < samples.size(); i++)
    in >> samples[i];
  return samples;
}

QVector<float> ReceiverSignalsInterface::read_samples_iq(QDataStream& in)
{
  quint32 count = 0;
  in >> count;
  QVector<float> samples;
  samples.resize(2*count);
  for (int i = 0; i < samples.size(); i++)
    in >> samples[i];
  return samples;
}
