﻿#include "ReceiverSignalsInputDevice.h"
#include "ReceiverSignalsInterface.h"

ReceiversSignalsInputDevice::ReceiversSignalsInputDevice(QObject* parent):
  QObject(parent)
{
  HostIsEnabled = false;
  ServerIsConnected = false;

  HostAddr = QHostAddress();
  HostPort = 7776;
  MulticastAddr = QHostAddress("239.192.1.1");
  MulticastPort = 7777;
}

#include <QHostInfo>

void ReceiversSignalsInputDevice::setHostAddr(QString addr)
{
  setHostAddr(QHostAddress(addr));
}

void ReceiversSignalsInputDevice::enable()
{
  if (HostIsEnabled) return;

  SocketIn = new QUdpSocket(this);
  SocketOut = new QUdpSocket(this);

  HostIsEnabled = true;
  connectToServer();  

  emit enabled();
}

void ReceiversSignalsInputDevice::disable()
{
  if (!HostIsEnabled) return;

  disconnectFromServer();
  HostIsEnabled = false;

  SocketIn->deleteLater();
  SocketOut->deleteLater();

  emit disabled();
}

//----------PRIVATE-------------

void ReceiversSignalsInputDevice::message(QString src, QString msg)
{
  qWarning("%s", QString("ReceiverSignalsInputDevice: %1: %2").arg(src).arg(msg).toLocal8Bit().data());
}

bool ReceiversSignalsInputDevice::connectToServer()
{
  if (ServerIsConnected) return false;

  SocketIn->abort();
  if (!SocketIn->bind(MulticastAddr, MulticastPort, QUdpSocket::ShareAddress|QUdpSocket::ReuseAddressHint))
  {
    SocketInErrorMessage("Connect To Server");
    return false;
  }
  if (!SocketIn->joinMulticastGroup(MulticastAddr))
  {
    SocketInErrorMessage("Connect To Server");
    return false;
  }
  connect(SocketIn, SIGNAL(readyRead()), this, SLOT(read()));

  sendQueryToHost(1);

  ServerIsConnected = true;
  return true;
}

void ReceiversSignalsInputDevice::disconnectFromServer()
{
  if (!ServerIsConnected) return;

  ServerIsConnected = false;

  sendQueryToHost(0);

  SocketIn->deleteLater();
}

void ReceiversSignalsInputDevice::sendQueryToHost(quint8 type)
{
  if (!HostIsEnabled)
    return;

  QByteArray buf;
  buf.append((quint8)255);
  buf.append(type);
  if (SocketOut->writeDatagram(buf, HostAddr, HostPort) != buf.size())
    SocketOutErrorMessage("Send query to Host");
}

void ReceiversSignalsInputDevice::sendQueryToServer(quint8 id, quint8 type, QByteArray data)
{
  if (!HostIsEnabled)
    return;

  QByteArray buf;
  buf.append(id);
  buf.append(type);
  if (!data.isEmpty())
    buf.append(data);
  if (SocketOut->writeDatagram(buf, HostAddr, HostPort) != buf.size())
    SocketOutErrorMessage("Send query to Server");
}

void ReceiversSignalsInputDevice::read()
{
  while(SocketIn->hasPendingDatagrams())
  {
    QByteArray buf;
    buf.resize(SocketIn->pendingDatagramSize());
    SocketIn->readDatagram(buf.data(), buf.size());

    quint8 id = buf.at(0), type = buf.at(1);
    buf.remove(0,2);
    emit ready_read(id, type, buf);
  }
}
