#include "ReceiverSignalsInputDriver.h"

ReceiverSignalsInputDriver::ReceiverSignalsInputDriver(QObject* parent) :
  QObject(parent)
{
  Device.moveToThread(&Thread);

  connect(&Thread, SIGNAL(started()), &Device, SLOT(enable()));
  connect(&Thread, SIGNAL(finished()), &Device, SLOT(disable()));
  connect(&Device, SIGNAL(disabled()), this, SLOT(disable()));
}

ReceiverSignalsInputDriver::~ReceiverSignalsInputDriver()
{
  foreach (ReceiverSignalsInterface* I, Interfaces)
    removeInterface(I);

  disable();
}

void ReceiverSignalsInputDriver::addInterface(ReceiverSignalsInterface* Interface)
{
  if (Interfaces.contains(Interface))
  {
    qWarning("ReceiverSignalsInputDriver: Interface #%d already added", Interface->getId());
    return;
  }

  Interfaces.append(Interface);

  Interface->setDevice(&Device);
  Interface->moveToThread(&Thread);

  connect(&Device, SIGNAL(ready_read(quint8,quint8,QByteArray)), Interface, SLOT(read(quint8,quint8,QByteArray)));
  connect(&Device, SIGNAL(disabled()), Interface, SLOT(disable()));
}

void ReceiverSignalsInputDriver::removeInterface(ReceiverSignalsInterface* Interface)
{
  if (!Interfaces.contains(Interface))
  {
    qWarning("ReceiverSignalsInputDriver: Interface #%d not added", Interface->getId());
    return;
  }

  disconnect(&Device, SIGNAL(ready_read(quint8,quint8,QByteArray)), Interface, SLOT(read(quint8,quint8,QByteArray)));
  disconnect(&Device, SIGNAL(disabled()), Interface, SLOT(disable()));

  Interfaces.removeOne(Interface);

  Interface->disable();
  Interface->setDevice(0);
}

void ReceiverSignalsInputDriver::enable(QString hostAddr)
{
  if (Thread.isRunning())
    return;

  Device.setHostAddr(hostAddr);
  Thread.start();
}

void ReceiverSignalsInputDriver::disable()
{
  if (Thread.isRunning())
  {
    Thread.quit();
    Thread.wait();
  }
}
