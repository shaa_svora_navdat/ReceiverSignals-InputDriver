#ifndef RECEIVERSIGNALSINTERFACE_H
#define RECEIVERSIGNALSINTERFACE_H

#include <QObject>
#include <QMetaType>
#include <QVector>
#include <QDataStream>

#include <ReceiverSignalsInputDevice.h>

#include "receiversignalsinputdevice_global.h"

Q_DECLARE_METATYPE(QVector<float>)

class RECEIVERSSIGNALSINPUTDEVICESHARED_EXPORT ReceiverSignalsInterface : public QObject
{
  Q_OBJECT

public:
  enum Query{queryEnableChannel, queryDisableChannel, queryGetConfig, querySetSignalEnabled, querySetSignalParams};
  enum Request{requestChannelEnabled = 0x80, requestChannelDisabled = 0x81, requestConfig = 0x82};

  enum SignalMode{modeDisable,modeStep,modeStream};
  struct CommonSignalParams{SignalMode mode;};

  explicit ReceiverSignalsInterface(quint8 ChannelId, ReceiversSignalsInputDevice* Device, QObject* parent = 0);

  bool isEnabled(){return isConnected;}

  void setDevice(ReceiversSignalsInputDevice* Device){dev = Device;}

  quint8 getId(){return Id;}

  template<typename T>
  static QVector<T> read_vector(QDataStream& in)
  {
    quint32 count = 0;
    in >> count;
    QVector<T> vector(count);
    for (quint32 i = 0; i < count; ++i)
      in >> vector[i];
    return vector;
  }

  static QVector<float> read_samples(QDataStream& in);
  static QVector<float> read_samples_iq(QDataStream& in);

public slots:
  void enable();
  void disable();

  void setSignalEnabled(quint8 signalType, bool enabled);
  void setSignalParams(quint8 signalType, ReceiverSignalsInterface::CommonSignalParams params, QByteArray spec_data = QByteArray());

  void read(quint8 id, quint8 type, QByteArray data);

signals:
  void enabled();
  void disabled();
  void data_readed();

private:
  quint8 Id;
  ReceiversSignalsInputDevice* dev;
  bool isConnected;

  virtual void read(quint8 type, QDataStream& in) = 0;
};

#endif // RECEIVERSIGNALSINTERFACE_H
